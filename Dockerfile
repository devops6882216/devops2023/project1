FROM scratch
ADD ubuntu.tar /
RUN apt-get -y update && apt-get -y install nginx
COPY default_conf /etc/nginx/sites-available/default
COPY app/* /usr/share/nginx/html/ 
EXPOSE 80
CMD ["/usr/sbin/nginx","-g","daemon off;"]

